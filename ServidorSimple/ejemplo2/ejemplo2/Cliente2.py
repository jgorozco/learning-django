'''
Created on 16 juil. 2012

@author: laurent
'''

class Cliente2(object):
   


    def __init__(self,numero,nombre,fecha,NIF):
        '''
        Constructor
        '''
        self.numero=numero
        self.nombre=nombre
        self.fecha=fecha
        self.NIF=NIF
    def __str__(self):
        return str(self.__dict__)
    
    @staticmethod
    def serialize(obj):
        return {
        "numero":   obj.numero,
        "nombre": obj.nombre,
        "fecha": obj.fecha,
        "NIF" : obj.NIF
        }

class Usuario(object):
    '''
    classdocs
    '''


    def __init__(self,nombre,pw,tipo,listaClientes):
        '''
        Constructor
        '''
        self.nombre=nombre
        self.pw=pw
        self.tipo=tipo
        self.listaClientes=listaClientes