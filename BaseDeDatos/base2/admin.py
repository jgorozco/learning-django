from base2.models import Doctor, Paciente, pacientesEnTratamiento, Tratamiento
from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.core import serializers
from django.http import HttpResponseRedirect, HttpResponse

class pacienteInLine(admin.TabularInline):
    model = Paciente
    
class doctorAdmin(admin.ModelAdmin):
    inlines = (pacienteInLine,)
    list_display = ('nombre', 'NIF',)
    
    
    #list_display_links = ('cod_cliente','cuenta')

    pass

class pacientesEnTratamientoInLine(admin.TabularInline):
    model = pacientesEnTratamiento
    extra = 1

class pacienteAdmin(admin.ModelAdmin):
    
    inlines = [pacientesEnTratamientoInLine]
    def export_as_json(self, request, queryset):
        response = HttpResponse(mimetype="text/javascript")
        serializers.serialize("json", queryset, stream=response)
        
        return response
        
    def export_selected_objects(self, request, queryset):        
        selected = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        ct = ContentType.objects.get_for_model(queryset.model)
        return HttpResponseRedirect("/export/?ct=%s&ids=%s" % (ct.pk, ",".join(selected)))

    def currido(self, request, queryset):
        rows_updated = queryset.update(status='ok')
        if rows_updated == 1:
            message_bit = "estado : currido"
        else:
            message_bit = "%s stories were" % rows_updated
        self.message_user(request, "%s successfully marked as published." % message_bit)
        
    def muerto(self, request, queryset):
        rows_updated = queryset.update(status='m')
        if rows_updated == 1:
            message_bit = "estado : muerto"
        else:
            message_bit = "%s stories were" % rows_updated
        self.message_user(request, "%s successfully marked as published." % message_bit)
        
    def enfermo(self, request, queryset):
        rows_updated = queryset.update(status='e')
        if rows_updated == 1:
            message_bit = "estado : muerto"
        else:
            message_bit = "%s stories were" % rows_updated
        self.message_user(request, "%s successfully marked as published." % message_bit)
     
 
    actions = [currido,muerto,export_as_json,export_selected_objects,enfermo]
    fieldsets = [
        (None,               {'fields': ['nombre','doctor']}),
        ('Date information', {'fields': ['DNI','fecha','status'], 'classes': ['collapse']}),
    ]    
    list_display = ('nombre', 'fecha','status','doctor', 'was_published_today')
    #list_filter = ('doctor',)
    
    pass

class TratamientoAdmin(admin.TabularInline):
     
    inlines = (pacientesEnTratamientoInLine)
    


admin.site.register(Doctor, doctorAdmin)
admin.site.register(Tratamiento)
#admin.site.register(pacientesDelDoctor)
admin.site.register(pacientesEnTratamiento)
admin.site.register(Paciente, pacienteAdmin)
