'''
Created on 17 juil. 2012

@author: laurent
'''

from django.contrib.databrowse.tests import SomeModel
from django.core import serializers
from django.core.context_processors import csrf
from django.forms.models import model_to_dict
from django.http import HttpResponse
from django.template import response
from django.utils import simplejson
from django.views.decorators.csrf import csrf_exempt
from models import Paciente, Doctor, Object
import datetime
import json





@csrf_exempt
def NormalQuery(request):
    if request.method == 'GET':
        return getMethod(request)
    else:
        try:
            return postMethod(request)   
        except Exception:
            HttpResponse("Error en la query")

def loginUser(request):
    
    paciente3 = Paciente.objects.create(nombre="Ringo Starr")
     
    
    #pacientes=[paciente1,paciente2]
    #doctor=Doctor('doctor1','pwDoctor','nmbre','5465465',pacientes)
    
    
    user=request.GET.get('user','')
    passw=request.GET.get('pass','')
    
    doctores=Doctor.objects.filter(login='doctor1')
    return HttpResponse(str(paciente3))
    '''
    if len(doctores)==0:
        return "ERROR_NO_USER"
    else:
        doctor=doctores[0]
        if passw!=doctor.pw :
            return "error contrasena o usuario"
        else:
            return
'''
def getMethod(request):
    return HttpResponse("get method")

def postMethod(request):
    #recordar un post        
    dictWithData=simplejson.loads(request.raw_post_data)
    miPaciente=Object(dictWithData['pacienteNuevo'])
    try:
        Paciente.objects.create(nombre=miPaciente.nombre,DNI=miPaciente.DNI,fecha=miPaciente.fecha)
    except Exception:
            return HttpResponse("Error zerzerzer")    
    #buscar 1   
    miPacienteDeLaBaseDeDatos=Paciente.objects.filter(nombre='pepito')    
   
    
    # this gives you a list of dicts
    raw_data = serializers.serialize('python', miPacienteDeLaBaseDeDatos)
    # now extract the inner `fields` dicts
    actual_data = [d['fields'] for d in raw_data]
    # and now dump to JSON
    output = json.dumps(actual_data)
   
    return HttpResponse(output)

def testDateTime(request):
    return HttpResponse(str(datetime.date.today())=='2012-07-19')

def export(request):
    return HttpResponse("ok")