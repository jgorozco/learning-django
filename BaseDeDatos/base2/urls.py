from django.conf.urls import patterns, include, url
from django.contrib import admin
from miPagina import loginUser, NormalQuery,testDateTime, export

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'base2.views.home', name='home'),
    # url(r'^base2/', include('base2.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    url(r'^Login/',loginUser),
    url(r'^query/',NormalQuery),
    url(r'^date/',testDateTime),
    url(r'^export/',export),
    url(r'^admin/', include(admin.site.urls)),
)

