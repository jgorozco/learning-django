from django.contrib.auth.models import User
from django.db import models
import datetime

'''
Created on 17 juil. 2012

@author: laurent
'''
STATUS_CHOICES = (
    ('e', 'Enfermo'),
    ('ok', 'BuenaSalud'),
    ('m', 'muerto'),
)


class Object():
    def __init__(self, d):
        for a, b in d.items():
            if isinstance(b, (list, tuple)):
                setattr(self, a, [Object(x) if isinstance(x, dict) else x for x in b])
            else:
                setattr(self, a, Object(b) if isinstance(b, dict) else b)
               

class Doctor(models.Model):
    
    login = models.ForeignKey(User)#deberia ser el objeto de user
    nombre= models.CharField(max_length=30)
    NIF= models.CharField(max_length=30)
    
    def __str__(self):
        return self.nombre+' ('+self.NIF+')' 

class Paciente(models.Model):
    nombre = models.CharField(max_length=30)
    DNI = models.CharField(max_length=30)
    fecha = models.CharField(max_length=30)
    status = models.CharField(max_length=2, choices=STATUS_CHOICES)  
    doctor = models.ForeignKey(Doctor)
    def __str__(self):
        return self.nombre+self.DNI
    def was_published_today(self):
        return self.fecha == str(datetime.date.today())
    was_published_today.short_description = u'Publicado hoy ?'

           
     
class Tratamiento(models.Model):
    nombre = models.CharField(max_length=30)
    duracion = models.CharField(max_length=30)
    precio = models.CharField(max_length=30)
    paciente = models.ManyToManyField(Paciente, through='pacientesEnTratamiento')
    def __str__(self):
        return self.nombre+'dura'+self.duracion+'y cuesta'+self.precio
'''    
class pacientesDelDoctor(models.Model):
    doctor = models.ForeignKey(Doctor)
    paciente = models.ForeignKey(Paciente)    
    fechaDeInicio = models.CharField(max_length=15)
    nombreDeVisitas = models.CharField(max_length=3)
    def __str__(self):
        return str(self.paciente)+' del doctor : '+str(self.doctor)
'''   
class pacientesEnTratamiento(models.Model):
    paciente = models.ForeignKey(Paciente)
    tratamiento = models.ForeignKey(Tratamiento)
    fechaDeInicio = models.CharField(max_length=15)
    reduccion = models.CharField(max_length=10)
    def __str__(self):
        return str(self.paciente)+' que trantan con'+str(self.tratamiento)
    