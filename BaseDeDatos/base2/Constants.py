'''
Created on 18 juil. 2012

@author: laurent
'''

ERROR_BAD_REQUEST='{"error": "BAD_REQUEST"}'
ERROR_BAD_CLIENT='{"error": "BAD_CLIENT"}'
ERROR_NO_CONTRACT='{"error": "NOC_CONTRACT"}'
ERROR_BAD_UNEXPECTED='{"error": "UNEXPECTED"}'
ERROR_BAD_USER='{"error": "BAD_USER"}'
ERROR_NO_USER='{"error": "NO_USER"}'
ERROR_BAD_DATA_POST='{"error":"BAD_DATA_POST"}'
RESULT_OK='{"result": "OK"}'
RESULT_ARCHIEVEMENT='{"result": "ARCHIEVEMENT"}'
